/*
 * File:   newmain.c
 * Author: Brandy
 * 
 * Created on March 13, 2016, 12:38 PM
 */


#include <xc.h>
#include "Alteri.h"

#define SPI_CLK TRISBbits.TRISB1
#define SPI_SDI TRISBbits.TRISB0
#define SPI_SDO TRISCbits.TRISC7
#define SPI_SS  TRISAbits.TRISA5

void SPI_Init(void);
void SPI_Write(char data);

void main(void) {
    TRISD = 0;
//    OpenSPI(SPI_FOSC_4||SSPENB,MODE_00,SMPMID);
    SPI_Init();
    while(1){
        int i; 
        for (i = 0; i < 255; i++) {
            LATD = i;
            SPI_Write(i);
            //delay_ms(10);
        } 
        
//        WriteSPI('A');
        //__delay_ms(500);        
    }
    return;
}

void SPI_Init(){
    //set tris bits for serial port pins
    SPI_CLK = 0; //output
    SPI_SDI = 1; //input
    SPI_SDO = 0; //output
    SPI_SS = 0; //output

    PIR1bits.SSPIF = 0; // Clear interrupt flag
    PIE1bits.SSPIE = 0; // Disable MSSP interrupt
    //INTCONbits.PEIE = 0; // Disable perriferal interrupts


    SSPSTATbits.SMP = 0; //input samled at middle of interval
    SSPSTATbits.CKE = 0; //Data transmitted from low to high clock
    
    //Enable SSP, Master mode, clock Fosc/4
    SSPCON1 |= 0b00100010;

}

void SPI_Write(char data){
    unsigned char TempVar;
    TempVar = SSPBUF; // Clears BF
    PIR1bits.SSPIF = 0; // Clear interrupt flag
    SSPBUF = data; // write byte to SSP1BUF register
    while(!PIR1bits.SSPIF) {}; // wait until bus cycle complete
        //return ( 0 ); // if WCOL bit is not set return non-negative#
}
