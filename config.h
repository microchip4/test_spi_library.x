/* 
 * File:   config.h
 * Author: Brandy
 *
 * Created on March 13, 2016, 12:37 PM
 */

#ifndef CONFIG_H

    #define _XTAL_FREQ 20000000
    #pragma config CPUDIV   = OSC1_PLL2
    #pragma config FOSC     = HS
    #pragma config PLLDIV = 5,USBDIV=2,FCMEN= OFF,IESO=OFF,PWRT=OFF,BOR=ON
    #pragma config BORV=3,VREGEN=ON,WDT=OFF,WDTPS=32768,MCLRE=ON,LPT1OSC=OFF
    #pragma config PBADEN=OFF,STVREN=ON,LVP=OFF,XINST=OFF,CP0=OFF,CP1=OFF
    #pragma config CPB=OFF,WRT0=OFF,WRT1=OFF,WRTB=OFF,WRTC=OFF
    #pragma config EBTR0=OFF,EBTR1 = OFF,EBTRB = OFF

#define	CONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */

